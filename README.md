Introduction
============

A brief description of the code goes here.

The code is built using terrafrom and ansible. 
    Why: Because HR asked to use these two technologise which might be geeting used in your current orgainisation. where in this could have been done by just using ansible, terrafrom or cloudformation.

How it works
-------------

To execeute this code you need to have terrafrom version 0.12 or  grater as defined in version.tf file. terrafrom init, plan and apply command can be executed after moving inside the DEKATLIS folder.

What it does
------------
  one the terrafrom apply command it executed. based on the definations provided in the variables perform below step
    1. Creates and ec2 instance on aws which is free tier
    2. Creates a dynamic invetry for ansible which it is going to use to connect to the to ec2 instance.
    3. execites the ansible playbook which configures elasticsearch on instance.

For secuing the communication and autontication based login
------------------------------------------------------------
  Elasicesearch xpack server is utilized to enable authentication and ssl secure connection.
      xpack.security.enabled: true
      xpack.security.ssl.diagnose.trust: true

what could have been done better/ extend the solution to launch
--------------------------------
  Utilization of aws service to build whole stack from scratch rather then just building single instance
    i.e
      aws_subnet
      aws_ebs_volume
      aws_security_group
      aws_elb_service_account
      aws_route53_zone
      aws_route
      aws_route_table

      Build of more robuse anaible playbook to accomadate different kind of OS with multiple condition to enhance reusability.

replace a running ElasticSearch instance
----------------------------------------
I don't think we need to replace the inscate rather i would have support to build this solution on top of docker and deploy it on ECS or EKS which would have helped us to do the deployment without downtime either by using blue/green deployment of by doing canary deployment in draining format.

What sacrifices did you make
----------------------------
As mention above if time would have permitted me (this is buit in 2-3 hours as on 3rd and 4th we all where facing power outage) i would have tried to build this solution on top of docker and try to deploy it on top of ecs ok eks.



