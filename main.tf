resource "aws_instance" "elasticsearch" {
  ami             = "ami-085925f297f89fce1"
  instance_type   = "t2.micro"
  subnet_id       = "	subnet-b4ebdaff"
  key_name        = "devapp_key_2272018"
  security_groups = ["sg-068851c791f4b7370"]

    # root_block_device {
    #  volume_size = "${var.RootVolume}"
    # }
    # ebs_block_device {
    #   volume_size = "${var.EbsVolume}"
    #   device_name = "/dev/xvda"
    # }
  tags = {
    Name = "elasticsearch"
  }
}

resource "null_resource" "create_hostfile" {
  depends_on = [aws_instance.elasticsearch]
  provisioner  "local-exec" {
    command = <<EOT
        echo [default] > hosts
        echo ${aws_instance.elasticsearch.private_ip} >> hosts
        echo [default:vars] >> hosts
        echo ansible_ssh_user=${var.ssh_user_name} >> hosts
        echo ansible_ssh_private_key_file=${var.ssh_key_path} >> hosts
        mv hosts ./ansible/    
EOT

  }
  
}

resource "null_resource" "execute-ansible" {

  depends_on = [null_resource.create_hostfile]
  provisioner "local-exec" {
    command = <<EOT
        sleep 120
        ANSIBLE_HOST_KEY_CHECKING=False
        ansible-playbook -i ./ansible/hosts ./ansible/main.yml    
EOT
}
}
