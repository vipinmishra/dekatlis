variable "aws_access_key" {
  description = "Access key"
  default     = " "
}

variable "aws_secret_key" {
  description = "secret key"
  default     = ""
}

variable "region" {
  default     = "us-east-1"
  description = "Region"
}

variable "ssh_user_name" {
  default     = ""
  description = "username"
}

variable "ssh_key_path" {
  default     = ""
  description = "key path"
}

